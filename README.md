# Fleeting Plugin Azure compute

This is a go plugin for fleeting on Microsoft Azure. It is intended to be run by
[fleeting](https://gitlab.com/gitlab-org/fleeting/fleeting), and cannot be run directly.

